import { Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { CounterActions } from '../../core/store/counter/counter.actions';
import { counterFeature, selectValue } from '../../core/store/counter/counter.reducer';

@Component({
  selector: 'app-counter',
  standalone: true,
  imports: [],
  template: `

    <h1>Total boxes: {{counter()}}</h1>
    <div>Total products: {{total()}}</div>
    <button (click)="decrement()">-</button>
    <button (click)="increment()">+</button>
    <button (click)="updateItemPerBox(10)">10</button>
    <button (click)="updateItemPerBox(20)">20</button>
  `,
  styles: ``
})
export default class CounterComponent {
  store = inject(Store)
  counter = this.store.selectSignal(selectValue)
  total = this.store.selectSignal(counterFeature.selectTotal)

  updateItemPerBox(value: number) {
    this.store.dispatch(CounterActions.updateItemsPerBox({  value }))
  }
  increment() {
    this.store.dispatch(CounterActions.increment({  value: 1 }))
  }
  decrement() {
    this.store.dispatch(CounterActions.decrement())
  }
}
