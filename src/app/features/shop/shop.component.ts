import { Component, inject, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductsActions } from './store/products/products.actions';
import { productsFeature, selectList } from './store/products/products.feature';

@Component({
  selector: 'app-shop',
  standalone: true,
  imports: [],
  template: `
    <p>
      shop works!
    </p>
    @for (product of products(); track product.id) {
      <li>{{product.name}}</li>
    }
  `,
  styles: ``
})
export default class ShopComponent implements OnInit {
  store = inject(Store)
  products = this.store.selectSignal(productsFeature.selectList)

  ngOnInit() {
    this.store.dispatch(ProductsActions.load())
    // dispatch getProduct
    // dispatch getProductSuccess(....)
  }
}
