import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, mergeMap, of } from 'rxjs';
import { selectValue } from '../../../../core/store/counter/counter.reducer';
import { Product } from '../../../../model/product';
import { ProductsActions } from './products.actions';

export const loadProducts = createEffect((
  actions = inject(Actions),
  store = inject(Store),
  http = inject(HttpClient)
) => {
  // ...
  return actions
    .pipe(
      ofType(ProductsActions.load),
      concatLatestFrom(() => [store.select(selectValue)]),
      mergeMap(([action, value]) => {
        return http.get<Product[]>('http://localhost:3000/productsX')
          .pipe(
            map(items => ProductsActions.loadSuccess({ items })),
            catchError(error => of(ProductsActions.loadFail()))
          )
      })
      // map(() => ProductsActions.loadSuccess({ items: [] }))
    )
}, { functional: true })






