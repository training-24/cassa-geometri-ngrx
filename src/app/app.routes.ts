// app.routes.ts
import { Routes } from '@angular/router';
import { provideState } from '@ngrx/store';
import { productsFeature } from './features/shop/store/products/products.feature';

export const routes: Routes = [
  {
    path: 'shop', loadComponent: () => import('./features/shop/shop.component'),
    providers: [
      provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
    ]
  },
  { path: 'cart', loadComponent: () => import('./features/cart/cart.component')},
  { path: 'counter', loadComponent: () => import('./features/counter/counter.component')},
  { path: '', redirectTo: 'shop', pathMatch: 'full' }
];
