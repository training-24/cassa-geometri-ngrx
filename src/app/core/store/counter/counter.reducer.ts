import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { CounterActions } from './counter.actions';

type CounterState = {
  value: number;
  itemsPerBox: number;
}
const initialState: CounterState = { value: 0, itemsPerBox: 10 };

export const counterFeature = createFeature({
  name: 'counter',
  reducer: createReducer(
    initialState,
    on(CounterActions.increment, (state, action) => ({...state,  value: state.value + action.value })),
    on(CounterActions.decrement, (state):CounterState => ({ ...state, value: state.value - 1})),
    on(CounterActions.updateItemsPerBox, (state, action):CounterState => ({ ...state, itemsPerBox: action.value })),
  ),
  extraSelectors: ({ selectValue, selectItemsPerBox}) => ({
    selectTotal: createSelector(
      selectValue,
      selectItemsPerBox,
      (counter, itemsPerBox) => counter * itemsPerBox
    )
  })
})

export const {
  selectValue,
  selectItemsPerBox,
  selectTotal
} = counterFeature;

