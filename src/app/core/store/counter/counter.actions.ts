import { createActionGroup, emptyProps, props } from '@ngrx/store';


/*

export const increment = createAction('[counter] increment', props<{ value: number }>())
export const decrement = createAction('[counter] decrement')
*/

export const CounterActions = createActionGroup({
  source: 'Counter',
  events: {
    increment: props<{ value: number }>(),
    decrement: emptyProps(),
    reset: emptyProps,
    updateItemsPerBox: props<{ value: number }>(),
  }
})


/*

export const increment = (val: number) => {
  return {
    type: 'increment',
    payload: val
  }
}
*/
