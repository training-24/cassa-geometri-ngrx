// app.component.ts
import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, AsyncPipe],
  template: `
    <button routerLink="shop">shop</button>
    <button routerLink="cart">cart</button>
    <button routerLink="counter">demo-counter</button>
    <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {


}
